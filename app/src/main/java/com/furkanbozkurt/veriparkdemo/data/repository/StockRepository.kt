package com.furkanbozkurt.veriparkdemo.data.repository

import android.os.Build
import com.furkanbozkurt.veriparkdemo.data.local.dao.StockDao
import com.furkanbozkurt.veriparkdemo.data.remote.StockService
import com.furkanbozkurt.veriparkdemo.model.*
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.PERIOD_ALL
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.PERIOD_DECREASING
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.PERIOD_INCREASING
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.PERIOD_VOLUME100
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.PERIOD_VOLUME30
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.PERIOD_VOLUME50
import com.furkanbozkurt.veriparkdemo.util.CryptoHelper
import com.furkanbozkurt.veriparkdemo.util.State
import com.furkanbozkurt.veriparkdemo.util.TokenInterceptor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@ExperimentalCoroutinesApi
class StockRepository @Inject constructor(
    private val stockDao: StockDao,
    private val stockService: StockService
) {

    @Inject
    lateinit var tokenInterceptor: TokenInterceptor

    fun getAuthorizationInfo(): Flow<State<AuthInfo>> {
        val deviceInfo = DeviceInfo()
        deviceInfo.deviceId = UUID.randomUUID().toString()
        deviceInfo.systemVersion = Build.VERSION.RELEASE
        deviceInfo.deviceModel = Build.MODEL
        deviceInfo.manifacturer = Build.MANUFACTURER
        deviceInfo.platformName = "Android"

        return flow<State<AuthInfo>> {
            emit(State.loading())
            val authInfo = stockService.getAuthorizationKey(deviceInfo)
            CryptoHelper.getInstance().setAesKey(authInfo.aesKey)
            CryptoHelper.getInstance().setAesIVKey(authInfo.aesIV)
            tokenInterceptor.token = authInfo.authorization
            emit(State.success(authInfo))
        }.flowOn(Dispatchers.Default)
            .catch { e ->
                emit(State.error(e.toString()))
            }
    }


    fun getStockList(period: String): Flow<State<List<Stock>>> {
        return object : NetworkBoundRepository<List<Stock>, StockResponse>() {
            override suspend fun saveRemoteData(response: StockResponse) {
                when (period) {
                    PERIOD_ALL -> stockDao.deleteStocks()
                }
                for (stock in response.stocks) {
                    stock.symbol = CryptoHelper.getInstance().decrypt(stock.symbol)
                    when (period) {
                        PERIOD_INCREASING -> stock.isIncreasing = true
                        PERIOD_DECREASING -> stock.isDecreasing = true
                        PERIOD_VOLUME30 -> stock.isVolume30 = true
                        PERIOD_VOLUME50 -> stock.isVolume50 = true
                        PERIOD_VOLUME100 -> stock.isVolume100 = true
                    }
                    stockDao.insertStock(stock)
                }
            }

            override fun fetchFromLocal(): Flow<List<Stock>> {
                return when (period) {
                    PERIOD_ALL -> stockDao.getAllStocks()
                    PERIOD_INCREASING -> stockDao.getIncreasingStocks(isIncreasing = true)
                    PERIOD_DECREASING -> stockDao.getDecreasingStocks(isDecreasing = true)
                    PERIOD_VOLUME30 -> stockDao.getVolume30Stocks(isVolume30 = true)
                    PERIOD_VOLUME50 -> stockDao.getVolume50Stocks(isVolume50 = true)
                    PERIOD_VOLUME100 -> stockDao.getVolume100Stocks(isVolume100 = true)
                    else -> stockDao.getAllStocks()
                }
            }

            override suspend fun fetchFromRemote(): Response<StockResponse> {
                val stockRequestBody = StockRequestBody()
                stockRequestBody.period = CryptoHelper.getInstance().encrypt(period)
                return stockService.getStockList(stockRequestBody)
            }

        }.asFlow().flowOn(Dispatchers.IO)
    }


    fun getStockDetails(stockId: Int): Flow<State<StockDetail>> {
        val stockRequestBody = StockRequestBody()
        stockRequestBody.id = CryptoHelper.getInstance().encrypt("$stockId")

        return flow<State<StockDetail>> {
            emit(State.loading())
            val stockDetail = stockService.getStockDetails(stockRequestBody)
            stockDetail.symbol = CryptoHelper.getInstance().decrypt(stockDetail.symbol)
            emit(State.success(stockDetail))
        }.flowOn(Dispatchers.Default)
            .catch { e ->
                emit(State.error(e.toString()))
            }
    }

}