package com.furkanbozkurt.veriparkdemo.data.local

import androidx.room.TypeConverter
import com.furkanbozkurt.veriparkdemo.model.Stock
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object DbTypeConverters {

    @JvmStatic
    @TypeConverter
    fun fromStockList(stock: List<Stock>?): String? {
        if (stock == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<Stock>?>() {}.type
        return gson.toJson(stock, type)
    }


    @JvmStatic
    @TypeConverter
    fun toStockList(stockString: String?): List<Stock>? {
        if (stockString == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<Stock>?>() {}.type
        return gson.fromJson<List<Stock>>(stockString, type)
    }

}