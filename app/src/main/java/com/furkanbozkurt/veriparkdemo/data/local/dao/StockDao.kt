package com.furkanbozkurt.veriparkdemo.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.furkanbozkurt.veriparkdemo.model.Stock
import kotlinx.coroutines.flow.Flow

@Dao
interface StockDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStocks(stocks: List<Stock>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStock(stock: Stock)

    @Query("SELECT * FROM ${Stock.TABLE_NAME}")
    fun getAllStocks(): Flow<List<Stock>>

    @Query("SELECT * FROM ${Stock.TABLE_NAME} WHERE isIncreasing = :isIncreasing")
    fun getIncreasingStocks(isIncreasing: Boolean): Flow<List<Stock>>

    @Query("SELECT * FROM ${Stock.TABLE_NAME} WHERE isDecreasing = :isDecreasing")
    fun getDecreasingStocks(isDecreasing: Boolean): Flow<List<Stock>>

    @Query("SELECT * FROM ${Stock.TABLE_NAME} WHERE isVolume30 = :isVolume30")
    fun getVolume30Stocks(isVolume30: Boolean): Flow<List<Stock>>

    @Query("SELECT * FROM ${Stock.TABLE_NAME} WHERE isVolume50 = :isVolume50")
    fun getVolume50Stocks(isVolume50: Boolean): Flow<List<Stock>>

    @Query("SELECT * FROM ${Stock.TABLE_NAME} WHERE isVolume100 = :isVolume100")
    fun getVolume100Stocks(isVolume100: Boolean): Flow<List<Stock>>

    @Query("delete from ${Stock.TABLE_NAME}")
    fun deleteStocks()
}