package com.furkanbozkurt.veriparkdemo.data.remote

import com.furkanbozkurt.veriparkdemo.model.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface StockService {

    @POST("/api/handshake/start")
    suspend fun getAuthorizationKey(@Body deviceInfo: DeviceInfo): AuthInfo

    @POST("/api/stocks/list")
    suspend fun getStockList(@Body stockRequestBody: StockRequestBody): Response<StockResponse>

    @POST("/api/stocks/detail")
    suspend fun getStockDetails(@Body stockRequestBody: StockRequestBody): StockDetail

}