package com.furkanbozkurt.veriparkdemo.model


data class StockDetail(
    val bid: Double,
    val channge: Double,
    val count: Int,
    val difference: Double,
    val graphicData: List<GraphicData>,
    val highest: Double,
    val isDown: Boolean,
    val isUp: Boolean,
    val lowest: Double,
    val maximum: Double,
    val minimum: Double,
    val offer: Double,
    val price: Double,
    var symbol: String,
    val volume: Double
) {
    data class GraphicData(
        val day: Int,
        val value: Double
    )
}