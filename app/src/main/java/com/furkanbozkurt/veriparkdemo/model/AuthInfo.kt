package com.furkanbozkurt.veriparkdemo.model


import com.google.gson.annotations.SerializedName

data class AuthInfo(
    @SerializedName("aesIV")
    var aesIV: String = "",
    @SerializedName("aesKey")
    var aesKey: String = "",
    @SerializedName("authorization")
    var authorization: String = "",
    @SerializedName("lifeTime")
    var lifeTime: String = ""
)