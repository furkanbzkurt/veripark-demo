package com.furkanbozkurt.veriparkdemo.model


data class StockResponse(
    val stocks: List<Stock>
)
