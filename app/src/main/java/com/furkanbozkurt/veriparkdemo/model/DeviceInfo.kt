package com.furkanbozkurt.veriparkdemo.model


data class DeviceInfo(
    var deviceId: String = "",
    var deviceModel: String = "",
    var manifacturer: String = "",
    var platformName: String = "",
    var systemVersion: String = ""
)