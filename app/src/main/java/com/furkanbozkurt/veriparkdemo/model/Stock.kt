package com.furkanbozkurt.veriparkdemo.model


import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Stock.TABLE_NAME)
data class Stock(
    @PrimaryKey
    val id: Int,
    val bid: Double,
    val difference: Double,
    val isDown: Boolean,
    val isUp: Boolean,
    val offer: Double,
    val price: Double,
    var symbol: String,
    val volume: Double,
    var isIncreasing: Boolean = false,
    var isDecreasing: Boolean = false,
    var isVolume30: Boolean = false,
    var isVolume50: Boolean = false,
    var isVolume100: Boolean = false
) {
    companion object {
        const val TABLE_NAME = "stock"
    }
}