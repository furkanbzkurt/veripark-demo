package com.furkanbozkurt.veriparkdemo.model


data class StockRequestBody(
    var id: String = "",
    var period: String = ""
)