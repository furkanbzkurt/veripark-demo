package com.furkanbozkurt.veriparkdemo.di.module

import com.furkanbozkurt.veriparkdemo.data.remote.StockService
import com.furkanbozkurt.veriparkdemo.util.Constants
import com.furkanbozkurt.veriparkdemo.util.TokenInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton


@Module
class NetModule {

    @Provides
    @Singleton
    fun provideOkHttp(tokenInterceptor: TokenInterceptor): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val okHttpBuilder =
            OkHttpClient().newBuilder().addInterceptor(interceptor).addInterceptor(tokenInterceptor)
        return okHttpBuilder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): StockService {
        return Retrofit.Builder()
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                )
            )
            .baseUrl(Constants.BASE_URL)
            .client(okHttpClient)
            .build()
            .create(StockService::class.java)
    }

}