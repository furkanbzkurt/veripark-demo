package com.furkanbozkurt.veriparkdemo.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.furkanbozkurt.veriparkdemo.di.ViewModelKey
import com.furkanbozkurt.veriparkdemo.ui.initial.InitialViewModel
import com.furkanbozkurt.veriparkdemo.ui.stocklist.StockListViewModel
import com.furkanbozkurt.veriparkdemo.ui.stockdetails.StockDetailsViewModel
import com.furkanbozkurt.veriparkdemo.util.ViewModelProvidersFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

//@Suppress("unused")
@ExperimentalCoroutinesApi
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProvidersFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(InitialViewModel::class)
    abstract fun provideInitialViewModel(initialViewModel: InitialViewModel?): ViewModel?

    @Binds
    @IntoMap
    @ViewModelKey(StockListViewModel::class)
    abstract fun provideStockListViewModel(stockListViewModel: StockListViewModel?): ViewModel?

    @Binds
    @IntoMap
    @ViewModelKey(StockDetailsViewModel::class)
    abstract fun provideStockDetailsViewModel(stockDetailsViewModel: StockDetailsViewModel?): ViewModel?


}
