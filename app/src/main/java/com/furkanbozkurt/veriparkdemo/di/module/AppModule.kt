package com.furkanbozkurt.veriparkdemo.di.module

import android.app.Application
import android.content.Context
import com.furkanbozkurt.veriparkdemo.data.local.AppDatabase
import com.furkanbozkurt.veriparkdemo.data.local.dao.StockDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideAppContext(): Context = app

    @Provides
    @Singleton
    fun provideAppDatabase(app: Application) = AppDatabase.getInstance(app)

    @Provides
    @Singleton
    fun provideStockDao(appDb: AppDatabase): StockDao = appDb.stockDao()

}