package com.furkanbozkurt.veriparkdemo.di.module;


import com.furkanbozkurt.veriparkdemo.ui.initial.InitialFragment;
import com.furkanbozkurt.veriparkdemo.ui.stocklist.StockListFragment;
import com.furkanbozkurt.veriparkdemo.ui.stockdetails.StockDetailsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract InitialFragment contributeInitialFragment();

    @ContributesAndroidInjector
    abstract StockListFragment contributeStockListFragment();

    @ContributesAndroidInjector
    abstract StockDetailsFragment contributeStockDetailsFragment();

}
