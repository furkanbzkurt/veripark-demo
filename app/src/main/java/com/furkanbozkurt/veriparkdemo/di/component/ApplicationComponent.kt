package com.furkanbozkurt.veriparkdemo.di.component

import com.furkanbozkurt.veriparkdemo.MainApp
import com.furkanbozkurt.veriparkdemo.di.module.ActivityBuilder
import com.furkanbozkurt.veriparkdemo.di.module.AppModule
import com.furkanbozkurt.veriparkdemo.di.module.NetModule
import com.furkanbozkurt.veriparkdemo.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),
        (AndroidSupportInjectionModule::class),
        (AppModule::class),
        (ActivityBuilder::class),
        (NetModule::class),
        (ViewModelModule::class)]
)

interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(mainApp: MainApp): Builder

        fun appModule(appModule: AppModule): Builder

        fun build(): ApplicationComponent
    }

    fun inject(mainApp: MainApp)
}