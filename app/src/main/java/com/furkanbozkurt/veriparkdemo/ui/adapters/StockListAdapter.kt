/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.furkanbozkurt.veriparkdemo.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.furkanbozkurt.veriparkdemo.databinding.ListItemStockBinding
import com.furkanbozkurt.veriparkdemo.model.Stock
import com.furkanbozkurt.veriparkdemo.util.Utils


class StockListAdapter(private val stockClickListener: StockClickListener) :
    ListAdapter<Stock, RecyclerView.ViewHolder>(StockDiffCallback()) {

    private lateinit var copyStockList: MutableList<Stock>
    private var charSearched = ""

    interface StockClickListener {
        fun onStockClick(stock: Stock)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return StockViewHolder(
            ListItemStockBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val city = getItem(position)
        (holder as StockViewHolder).bind(city, stockClickListener, position)
    }

    fun filter(charText: String) {
        this.charSearched = charText
        this.charSearched = Utils.filterModeToString(charText).toString()
        var filteredList: MutableList<Stock> = mutableListOf()
        if (this.charSearched.length == 0) {
            if (::copyStockList.isInitialized) {
                filteredList.addAll(copyStockList)
            }
        } else {
            if (::copyStockList.isInitialized) {
                for (stock in copyStockList) {
                    if (Utils.filterModeToString(stock.symbol)?.contains(charText)!!) {
                        filteredList.add(stock)
                    }
                }
            }
        }
        submitList(filteredList)
    }

    fun setList(radioList: List<Stock>) {
        copyStockList = mutableListOf<Stock>()
        copyStockList.addAll(radioList)
        filter(if (charSearched.isEmpty()) "" else charSearched)
    }

    class StockViewHolder(
        private val binding: ListItemStockBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Stock,
            stockClickListener: StockClickListener,
            position: Int
        ) {
            binding.apply {
                stock = item
                isOddOrder = position % 2 == 1
                executePendingBindings()
            }

            binding.linearLayoutStockInfo.setOnClickListener {
                stockClickListener.onStockClick(item)
            }
        }
    }
}

private class StockDiffCallback : DiffUtil.ItemCallback<Stock>() {

    override fun areItemsTheSame(oldItem: Stock, newItem: Stock): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Stock, newItem: Stock): Boolean {
        return oldItem == newItem
    }
}