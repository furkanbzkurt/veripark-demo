package com.furkanbozkurt.veriparkdemo.ui.stocklist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.veriparkdemo.data.repository.StockRepository
import com.furkanbozkurt.veriparkdemo.model.Stock
import com.furkanbozkurt.veriparkdemo.util.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class StockListViewModel @Inject constructor(
    private val stockRepository: StockRepository
) : ViewModel() {

    private val _stocksLiveData = MutableLiveData<State<List<Stock>>>()
    val stocksLiveData: LiveData<State<List<Stock>>>
        get() = _stocksLiveData


    fun getStockList(period: String) {
        viewModelScope.launch {
            stockRepository.getStockList(period).collect {
                _stocksLiveData.postValue(it)
            }
        }
    }
}