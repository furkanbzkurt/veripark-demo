package com.furkanbozkurt.veriparkdemo.ui.initial

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.veriparkdemo.data.repository.StockRepository
import com.furkanbozkurt.veriparkdemo.model.AuthInfo
import com.furkanbozkurt.veriparkdemo.util.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject


@ExperimentalCoroutinesApi
class InitialViewModel @Inject constructor(
    private val stockRepository: StockRepository
) : ViewModel() {

    private val _authInfoLiveData = MutableLiveData<State<AuthInfo>>()
    val authInfoLiveData: LiveData<State<AuthInfo>>
        get() = _authInfoLiveData


    fun getAuthInfo() {
        viewModelScope.launch {
            stockRepository.getAuthorizationInfo().collect {
                _authInfoLiveData.postValue(it)
            }
        }
    }

}