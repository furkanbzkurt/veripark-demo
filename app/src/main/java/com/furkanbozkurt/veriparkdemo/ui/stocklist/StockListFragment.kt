package com.furkanbozkurt.veriparkdemo.ui.stocklist

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.furkanbozkurt.veriparkdemo.R
import com.furkanbozkurt.veriparkdemo.databinding.FragmentStockListBinding
import com.furkanbozkurt.veriparkdemo.model.Stock
import com.furkanbozkurt.veriparkdemo.ui.adapters.StockListAdapter
import com.furkanbozkurt.veriparkdemo.util.*
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.ANIMATION_DURATION
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
class StockListFragment : Fragment(), StockListAdapter.StockClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    @Inject
    lateinit var appContext: Context

    private lateinit var viewModel: StockListViewModel
    private lateinit var binding: FragmentStockListBinding
    private val args: StockListFragmentArgs by navArgs()
    private var sharedViewModel: SharedViewModel? = null

    private val mAdapter: StockListAdapter by lazy {
        StockListAdapter(
            stockClickListener = this
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentStockListBinding.inflate(inflater, container, false)

        init()
        setHideKeyboardOnRecyclerTouch()
        initStocks()
        initSearchView()
        handleNetworkChanges()

        return binding.root
    }

    private fun init() {
        binding.loadingState = true
        viewModel = ViewModelProvider(this, viewModelFactory).get(StockListViewModel::class.java)
        activity?.let {
            sharedViewModel = ViewModelProvider(it).get(SharedViewModel::class.java)
        }

        binding.recyclerViewStocks.adapter = mAdapter
    }

    private fun initStocks() {
        viewModel.stocksLiveData.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is State.Loading -> {
                    binding.loadingState = true
                }
                is State.Success -> {
                    binding.loadingState = state.data.isEmpty()
                    if (state.data.isNotEmpty()) mAdapter.setList(state.data.toMutableList())
                }
                is State.Error -> {
                    binding.loadingState = false
                    showToast(getString(R.string.an_error_occurred))
                }
            }
        })

        if (viewModel.stocksLiveData.value !is State.Success) {
            viewModel.getStockList(args.periodType)
        }
    }

    private fun initSearchView() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                mAdapter.filter(newText)
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })
    }

    private fun navigateToDetails(stockId: Int) {
        sharedViewModel?.stateBurgerIconVisibility?.postValue(false)
        val direction =
            StockListFragmentDirections.actionStockDetailsFragment(stockId)
        findNavController().navigate(direction)
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun setHideKeyboardOnRecyclerTouch() {
        binding.recyclerViewStocks.setOnTouchListener(View.OnTouchListener { v, event ->
            Utils.hideKeyboard(requireActivity())
            false
        })
    }

    override fun onStockClick(stock: Stock) {
        Utils.hideKeyboard(requireActivity())
        navigateToDetails(stock.id)
    }

    private fun handleNetworkChanges() {
        NetworkUtils.getNetworkLiveData(appContext)
            .observe(viewLifecycleOwner, Observer { isConnected ->
                if (!isConnected) {
                    binding.textViewNetworkStatus.text = getString(R.string.title_not_connected)
                    binding.networkStatusLayout.apply {
                        show()
                        setBackgroundColor(context.getColorRes(R.color.colorStatusNotConnected))
                    }
                } else {
                    if (viewModel.stocksLiveData.value is State.Error) {
                        viewModel.getStockList(args.periodType)
                    }
                    binding.textViewNetworkStatus.text = getString(R.string.title_connected)
                    binding.networkStatusLayout.apply {
                        setBackgroundColor(context.getColorRes(R.color.colorStatusConnected))

                        animate()
                            .alpha(1f)
                            .setStartDelay(ANIMATION_DURATION)
                            .setDuration(ANIMATION_DURATION)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    hide()
                                }
                            })
                    }
                }
            })
    }
}
