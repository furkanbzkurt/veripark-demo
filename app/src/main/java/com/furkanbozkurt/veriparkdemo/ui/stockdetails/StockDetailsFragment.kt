package com.furkanbozkurt.veriparkdemo.ui.stockdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.furkanbozkurt.veriparkdemo.R
import com.furkanbozkurt.veriparkdemo.databinding.FragmentStockDetailsBinding
import com.furkanbozkurt.veriparkdemo.model.StockDetail
import com.furkanbozkurt.veriparkdemo.util.*
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
class StockDetailsFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    private lateinit var viewModel: StockDetailsViewModel
    private lateinit var binding: FragmentStockDetailsBinding
    private lateinit var lineChartHelper: LineChartHelper
    private var sharedViewModel: SharedViewModel? = null
    private val args: StockDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentStockDetailsBinding.inflate(inflater, container, false)

        init()
        handleBackPress()
        initChart()
        observeStockDetails()

        return binding.root
    }

    private fun init() {
        binding.loadingState = true
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(StockDetailsViewModel::class.java)
        activity?.let {
            sharedViewModel = ViewModelProvider(it).get(SharedViewModel::class.java)
        }
    }

    private fun handleBackPress() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            sharedViewModel?.stateBurgerIconVisibility?.postValue(true)
            findNavController().popBackStack()
        }
    }


    private fun observeStockDetails() {
        viewModel.stockDetailsLiveData.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is State.Loading -> {
                    binding.loadingState = true
                }
                is State.Success -> {
                    binding.stockDetails = state.data
                    setGraphicData(state.data.graphicData)
                    binding.loadingState = false
                }
                is State.Error -> {
                    binding.loadingState = false
                    showToast(getString(R.string.an_error_occurred))
                }
            }
        })

        viewModel.getSockDetails(args.stockId)
    }

    private fun initChart() {
        lineChartHelper = LineChartHelper(binding.lineChartMonthlyChange, context)
        lineChartHelper.initChart()
    }

    private fun setGraphicData(stockGraphicData: List<StockDetail.GraphicData>) {
        lineChartHelper.setData(stockGraphicData)
        lineChartHelper.validateChart()
    }

}
