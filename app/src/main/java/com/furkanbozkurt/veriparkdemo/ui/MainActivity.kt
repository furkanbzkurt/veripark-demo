package com.furkanbozkurt.veriparkdemo.ui

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.furkanbozkurt.veriparkdemo.R
import com.furkanbozkurt.veriparkdemo.databinding.ActivityMainBinding
import com.furkanbozkurt.veriparkdemo.util.SharedViewModel
import com.furkanbozkurt.veriparkdemo.util.Utils
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    private lateinit var binding: ActivityMainBinding
    private var sharedViewModel: SharedViewModel? = null

    private val navController by lazy {
        Navigation.findNavController(
            this,
            R.id.navHostFragment
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        sharedViewModel = ViewModelProvider(this).get(SharedViewModel::class.java)
        setupDrawerLayout()
    }

    private fun setupDrawerLayout() {
        binding.imageViewBurgerMenu.setOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }

        NavigationUI.setupWithNavController(binding.navigationView, navController)
        observeBurgerIconVisibility()


        val toggle = object : ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            R.string.app_name,
            R.string.app_name
        ) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                Utils.hideKeyboard(this@MainActivity)
                navController.popBackStack()
            }
        }
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun observeBurgerIconVisibility() {
        GlobalScope.launch(Dispatchers.Main) {
            sharedViewModel?.stateBurgerIconVisibility?.observe(this@MainActivity, Observer {
                if (it) binding.imageViewBurgerMenu.visibility = VISIBLE
                else binding.imageViewBurgerMenu.visibility = GONE
            })
        }

    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }
}
