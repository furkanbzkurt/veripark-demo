package com.furkanbozkurt.veriparkdemo.ui.stockdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.veriparkdemo.data.repository.StockRepository
import com.furkanbozkurt.veriparkdemo.model.StockDetail
import com.furkanbozkurt.veriparkdemo.util.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class StockDetailsViewModel @Inject constructor(
    private val stockRepository: StockRepository
) : ViewModel() {

    private val _stockDetailsLiveData = MutableLiveData<State<StockDetail>>()
    val stockDetailsLiveData: LiveData<State<StockDetail>>
        get() = _stockDetailsLiveData


    fun getSockDetails(stockId: Int) {
        viewModelScope.launch {
            stockRepository.getStockDetails(stockId).collect {
                _stockDetailsLiveData.postValue(it)
            }
        }
    }
}