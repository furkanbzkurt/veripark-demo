package com.furkanbozkurt.veriparkdemo.ui.initial

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.furkanbozkurt.veriparkdemo.R
import com.furkanbozkurt.veriparkdemo.databinding.FragmentInitialBinding
import com.furkanbozkurt.veriparkdemo.util.Constants.Companion.PERIOD_ALL
import com.furkanbozkurt.veriparkdemo.util.SharedViewModel
import com.furkanbozkurt.veriparkdemo.util.State
import com.furkanbozkurt.veriparkdemo.util.ViewModelProvidersFactory
import com.furkanbozkurt.veriparkdemo.util.showToast
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject


@ExperimentalCoroutinesApi
class InitialFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    private lateinit var viewModel: InitialViewModel
    private lateinit var binding: FragmentInitialBinding
    private var sharedViewModel: SharedViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentInitialBinding.inflate(inflater, container, false)

        init()
        setClickListeners()
        observeAuthState()

        return binding.root
    }

    private fun init() {
        viewModel = ViewModelProvider(this, viewModelFactory).get(InitialViewModel::class.java)
        activity?.let {
            sharedViewModel = ViewModelProvider(it).get(SharedViewModel::class.java)
        }
        binding.loadingState = false
    }

    private fun setClickListeners() {
        binding.buttonOpenStockList.setOnClickListener {
            viewModel.getAuthInfo()
        }
    }

    private fun observeAuthState() {
        viewModel.authInfoLiveData.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is State.Loading -> {
                    binding.loadingState = true
                }
                is State.Success -> {
                    navigateToStockList()
                }
                is State.Error -> {
                    binding.loadingState = false
                    showToast(getString(R.string.an_error_occurred))
                }
            }
        })
    }

    private fun navigateToStockList() {
        sharedViewModel?.stateBurgerIconVisibility?.postValue(true)
        val direction = InitialFragmentDirections.actionInitialFragment(PERIOD_ALL)
        view?.findNavController()?.navigate(direction)
    }

}
