package com.furkanbozkurt.veriparkdemo.util

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {
    val stateBurgerIconVisibility = MutableLiveData<Boolean>(false)
    val stateToolbarText = MutableLiveData<String>("")
}