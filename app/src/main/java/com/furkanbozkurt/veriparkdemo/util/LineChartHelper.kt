package com.furkanbozkurt.veriparkdemo.util

import android.content.Context
import android.graphics.Color
import android.graphics.DashPathEffect
import androidx.core.content.ContextCompat
import com.furkanbozkurt.veriparkdemo.R
import com.furkanbozkurt.veriparkdemo.model.StockDetail
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.Utils
import java.util.*

class LineChartHelper(private var lineChart: LineChart, private var context: Context?) {


    fun initChart() {
        lineChart.setBackgroundColor(Color.WHITE)
        lineChart.description.isEnabled = false
        lineChart.setTouchEnabled(true)
        lineChart.setDrawGridBackground(false)

        val mv = ChartMarkerView(context, R.layout.custom_marker_view)
        mv.chartView = lineChart
        lineChart.marker = mv
        lineChart.isDragEnabled = false
        lineChart.setScaleEnabled(false)
        lineChart.setPinchZoom(false)

        var xAxis: XAxis = lineChart.getXAxis()
        xAxis.enableGridDashedLine(10f, 10f, 0f)

        var yAxis: YAxis = lineChart.getAxisLeft()
        lineChart.getAxisRight().setEnabled(false)

        // horizontal grid lines
        yAxis.enableGridDashedLine(10f, 10f, 0f)

        // axis range
        yAxis.axisMaximum = 180f
        yAxis.axisMinimum = -20f

        val llXAxis = LimitLine(9f, "Index 10")
        llXAxis.lineWidth = 4f
        llXAxis.enableDashedLine(10f, 10f, 0f)
        llXAxis.labelPosition = LimitLine.LimitLabelPosition.RIGHT_BOTTOM
        llXAxis.textSize = 10f
        val ll1 = LimitLine(150f, "Upper Limit")
        ll1.lineWidth = 4f
        ll1.enableDashedLine(10f, 10f, 0f)
        ll1.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        ll1.textSize = 10f

        // draw limit lines behind data instead of on top
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        // add limit lines
        yAxis.addLimitLine(ll1)

        lineChart.getXAxis().setDrawLabels(false);
        lineChart.getXAxis().setDrawAxisLine(false);
    }

    fun setData(stockGraphicData: List<StockDetail.GraphicData>) {
        val values =
            ArrayList<Entry>()
        for (i in stockGraphicData) {
            values.add(
                Entry(
                    i.day.toFloat(),
                    i.value.toFloat()
                )
            )
        }
        val set1: LineDataSet
        if (lineChart.getData() != null &&
            lineChart.getData().getDataSetCount() > 0
        ) {
            set1 = lineChart.getData().getDataSetByIndex(0) as LineDataSet
            set1.values = values
            set1.notifyDataSetChanged()
            lineChart.getData().notifyDataChanged()
            lineChart.notifyDataSetChanged()
        } else {
            // create a dataset and give it a type
            set1 = LineDataSet(values, "DataSet 1")
            set1.setDrawIcons(false)

            // draw dashed line
            set1.enableDashedLine(10f, 5f, 0f)

            // black lines and points
            set1.color = Color.BLACK
            set1.setCircleColor(Color.BLACK)

            // line thickness and point size
            set1.lineWidth = 1f
            set1.circleRadius = 3f

            // draw points as solid circles
            set1.setDrawCircleHole(false)

            // customize legend entry
            set1.formLineWidth = 1f
            set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
            set1.formSize = 15f

            // text size of values
            set1.valueTextSize = 9f

            // draw selection line as dashed
            set1.enableDashedHighlightLine(10f, 5f, 0f)

            // set the filled area
            set1.setDrawFilled(true)
            set1.setDrawValues(false)
            set1.fillFormatter =
                IFillFormatter { dataSet, dataProvider -> lineChart.getAxisLeft().getAxisMinimum() }

            // set color of filled area
            if (Utils.getSDKInt() >= 18) {
                // drawables only supported on api level 18 and above
                val drawable = context?.let { ContextCompat.getDrawable(it, R.drawable.fade_red) }
                set1.fillDrawable = drawable
                //set1.fillColor = Color.RED
            } else {
                set1.fillColor = Color.BLACK
            }
            val dataSets = ArrayList<ILineDataSet>()
            dataSets.add(set1) // add the data sets

            // create a data object with the data sets
            val data = LineData(dataSets)

            // set data
            lineChart.setData(data)
        }
    }

    fun validateChart() {
        // draw points over time
        lineChart.animateX(1500)
        // get the legend (only possible after setting data)
        val l: Legend = lineChart.getLegend()
        // draw legend entries as lines
        l.form = Legend.LegendForm.LINE
        lineChart.getLegend().setEnabled(false);
    }
}