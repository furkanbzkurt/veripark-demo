package com.furkanbozkurt.veriparkdemo.util

class Constants {
    companion object {
        const val DATABASE_NAME = "veripark_demo_db"
        const val BASE_URL = "https://mobilechallenge.veripark.com/"
        const val PERIOD_ALL = "all"
        const val PERIOD_INCREASING = "increasing"
        const val PERIOD_DECREASING = "decreasing"
        const val PERIOD_VOLUME30 = "volume30"
        const val PERIOD_VOLUME50 = "volume50"
        const val PERIOD_VOLUME100 = "volume100"
        const val ANIMATION_DURATION = 1000L
    }
}