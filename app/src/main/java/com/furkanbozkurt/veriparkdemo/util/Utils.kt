package com.furkanbozkurt.veriparkdemo.util

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.util.*


object Utils {


    fun filterModeToString(target: String): String? {
        return try {
            target.replace('ç', 'c').replace('ü', 'u').replace('ş', 's').replace('ı', 'i')
                .replace('ğ', 'g').replace('ö', 'o').replace('Ğ', 'g').replace('Ç', 'c')
                .replace('Ü', 'u').replace('Ş', 'S').replace('I', 'i').replace('ı', 'i')
                .replace('İ', 'i').replace('Ö', 'o')
                .toLowerCase(Locale("tr_TR"))
        } catch (e: Exception) {
            e.printStackTrace()
            " "
        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}