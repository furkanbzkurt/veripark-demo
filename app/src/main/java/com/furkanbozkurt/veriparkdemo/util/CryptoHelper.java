package com.furkanbozkurt.veriparkdemo.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoHelper {
    String aesKey = "xxxxxxxxxxxxxxxxxxxx";
    String aesIV = "xxxxxxxxxxxxxxxx";

    private static CryptoHelper instance = null;

    public static CryptoHelper getInstance() {

        if (instance == null) {
            instance = new CryptoHelper();
        }
        return instance;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public void setAesIVKey(String aesIV) {
        this.aesIV = aesIV;
    }

    public String encrypt(String message) throws NoSuchAlgorithmException,
            NoSuchPaddingException, IllegalBlockSizeException,
            BadPaddingException, InvalidKeyException,
            UnsupportedEncodingException, InvalidAlgorithmParameterException {

        byte[] srcBuff = message.getBytes("UTF8");
        //here using substring because AES takes only 16 or 24 or 32 byte of key
        byte[] decodedKey = Base64.decode(aesKey, Base64.DEFAULT);
        SecretKeySpec skeySpec = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
        byte[] decodedIVKey = Base64.decode(aesIV, Base64.DEFAULT);
        IvParameterSpec ivSpec = new IvParameterSpec(decodedIVKey, 0, decodedIVKey.length);
        Cipher ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
        byte[] dstBuff = ecipher.doFinal(srcBuff);
        String base64 = Base64.encodeToString(dstBuff, Base64.DEFAULT);
        return base64;
    }

    public String decrypt(String encrypted) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, UnsupportedEncodingException {

        byte[] decodedKey = Base64.decode(aesKey, Base64.DEFAULT);
        SecretKeySpec skeySpec = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
        byte[] decodedIVKey = Base64.decode(aesIV, Base64.DEFAULT);
        IvParameterSpec ivSpec = new IvParameterSpec(decodedIVKey, 0, decodedIVKey.length);
        Cipher ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        ecipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
        byte[] raw = Base64.decode(encrypted, Base64.DEFAULT);
        byte[] originalBytes = ecipher.doFinal(raw);
        String original = new String(originalBytes, "UTF8");
        return original;
    }
}
